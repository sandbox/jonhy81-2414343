<?php

/**
 * @file
 * The default format for adresses.
 */

$plugin = array(
  'title' => t('VAT Number'),
  'format callback' => 'addressfield_format_vat_number_generate',
  'type' => 'vat',
  'weight' => 100,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_format_vat_number_generate(&$format, $address, $context = array()) {
  if ($context['mode'] == 'form') {
    $format['vat_block'] = array(
      '#type' => 'addressfield_container',
      '#attributes' => array('class' => array('addressfield-container-inline')),
      '#weight' => 200,
      // The addressfield is considered empty without a country, hide all fields
      // until one is selected.
      '#access' => !empty($address['country']),
    );
    $format['vat_block']['vat'] = array(
      '#title' => t('VAT Number'),
      '#size' => 15,
      '#attributes' => array('class' => array('vat')),
      '#type' => 'textfield',
      '#tag' => 'span',
      '#default_value' => isset($address['vat']) ? $address['vat'] : '',
      '#element_validate' => array('vat_number_field_widget_element_validate'),
    );
  }
  else {
    // Add our own render callback for the format view
    $format['#pre_render'][] = '_addressfield_vat_number_render_address';
  }
}
