<?php

/**
 * Callback for mapping. Here is where the actual mapping happens.
 *
 * When the callback is invoked, $target contains the name of the field the
 * user has decided to map to and $value contains the value of the feed item
 * element the user has picked as a source.
 */

function addressfield_vat_number_feeds_set_target($source, $entity, $target, $value) {
 list($field_name, $sub_field) = explode(':', $target, 2);

  // If the field is already set on the given entity, update the existing value
  // array. Otherwise start with a fresh field value array.
  $field = isset($entity->$field_name) ? $entity->$field_name : array();

  if (isset($field[LANGUAGE_NONE][0]['data'])) {
      $data = unserialize($field[LANGUAGE_NONE][0]['data']);
  }
  $data[$sub_field] = $value;
  $field[LANGUAGE_NONE][0]['data'] = serialize($data);

  $entity->$field_name = $field;
}

