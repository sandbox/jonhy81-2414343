<?php

/**
 * A Field Handler class that makes address_link subfield destination for migate api.
 *
 * Arguments are used to specify all link related values:
 *   'link_title',
 *   'link_url',
 */
class MigrateAddressVatNumberFieldHandler extends MigrateAddressFieldHandler {
  public function __construct() {
    parent::__construct();
  }
  /**
   * Provide subfields for the addressfield columns.
   */
  public function fields() {
    // Declare our arguments to also be available as subfields.
    $fields = array(
      'vat' => t('VAT Number'),
    );
    return $fields;
  }

  /**
   * Implements MigrateFieldHandler::prepare().
   *
   * @param $entity
   * @param array $field_info
   * @param array $instance
    * @param array $values
   *
   * @return null
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    $arguments = array();
    if (isset($values['arguments'])) {
      $arguments = array_filter($values['arguments']);
      unset($values['arguments']);
    }
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    return isset($return) ? $return : NULL;
  }
}
